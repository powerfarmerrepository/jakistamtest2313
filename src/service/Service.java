package service;

import org.testng.Assert;
import org.testng.annotations.Test;
import static utils.Methods.*;
import main.Main;
import pageObjects.LoginPage;
import pageObjects.MainPage;
import pageObjects.OperationsPage;


public class Service extends Main {
	
		
	@Test
	public void serviceTest() throws InterruptedException{
		
		
		/* Inicjacja PageObjects */
		
		LoginPage loginPage = new LoginPage(driver);		
		MainPage mainPage = new MainPage(driver);
		OperationsPage operationsPage = new OperationsPage(driver);
	   
		/* Koniec inicjacji PageObjects */
		
		if (parameters.hasParameter("result")) {
			
			if (parameters.getParameter("result").equals("fail")) {
							
			   Assert.assertTrue(false,"Wywolany blad");
			   
			}
			
		}
	
		report.logInfo("INFO WPIS");	
	    
		Thread.sleep(15000);
		//parameters.setParameter("test_wy","wartosc");		
		
	}
	
	
	
}
